#!/bin/bash
sudo apt update
sudo apt upgrade -y

sudo apt install crunch -y
sudo apt install john -y
sudo apt install nmap -y
sudo apt install exiftool -y
sudo apt install binwalk -y
sudo apt install foremost -y
sudo apt install pkcrack -y
sudo apt install libc6:i386 libncurses5:i386 libstdc++6:i386 -y
sudo apt install gcc-multilib g++-multilib -y
sudo apt install binutils -y
sudo apt install socat -y
sudo apt install git -y
sudo apt install kali-linux-pwtools -y
sudo apt install snort -y
dpkg-reconfigure locales -y
dpkg-reconfigure tzdata -y
dpkg-reconfigure keyboard-configuration -y
dpkg-reconfigure console-data -y
# radare2
git clone https://github.com/radare/radare2
sh radare2/sys/install.sh
# ROPガジェット用
wget https://github.com/0vercl0k/rp/releases/download/v1/rp-lin-x64 \
&& wget https://github.com/0vercl0k/rp/releases/download/v1/rp-lin-x86 \
&& chmod +x rp-lin-x64 rp-lin-x86 \
&& mv rp-lin-x64 rp-lin-x86 /usr/local/bin
# gdb-peda
git clone https://github.com/longld/peda.git ~/peda
echo "source ~/peda/peda.py" >> ~/.gdbinit

